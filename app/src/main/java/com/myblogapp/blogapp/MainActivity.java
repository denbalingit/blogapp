package com.myblogapp.blogapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.myblogapp.blogapp.Activities.BlogListActivity;
import com.myblogapp.blogapp.Models.User;
import com.myblogapp.blogapp.Utils.MyLogger;
import com.myblogapp.blogapp.Utils.RealmController;
import com.myblogapp.blogapp.Utils.UserUtil;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

  @Bind(R.id.registerBtn) Button registerBtn;
  @Bind(R.id.loginBtn) Button loginBtn;
  @Bind(R.id.logoutBtn) Button logoutBtn;

  @Bind(R.id.userName) EditText userName;
  @Bind(R.id.userPassword) EditText userPassword;



  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null)
            .show();
      }
    });

    registerBtn.setOnClickListener(this);
    loginBtn.setOnClickListener(this);
    logoutBtn.setOnClickListener(this);
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override public void onClick(View view) {
    switch (view.getId()){

      case R.id.registerBtn:
        registerUser();
        break;
      case R.id.loginBtn:
        loginUser();
        break;
      case R.id.logoutBtn:
        UserUtil.getInstance().removeLoggedUser();
        break;

    }
  }

  private void registerUser(){

    User newUser = new User(userName.getText().toString(),userPassword.getText().toString());
    RealmController.getInstance().save(newUser);

    Toast.makeText(this,"Successfully registered",Toast.LENGTH_SHORT).show();
    userName.setText("");
    userPassword.setText("");



  }

  private void loginUser(){

    String name = userName.getText().toString();
    String password = userPassword.getText().toString();

    User loggedUser = RealmController.getInstance().getRealm().where(User.class).equalTo("name",name).equalTo("password",password).findFirst();

    UserUtil.getInstance().setLoggedUser(loggedUser.getName());

    if(loggedUser!= null){
      MyLogger.showLog("loggedUser : "+loggedUser.getName());

      //Go to Users Blog list
      startActivity(new Intent(this,BlogListActivity.class));

    } else {
      MyLogger.showLog("does not exist");
    }

  }
}
