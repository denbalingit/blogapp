package com.myblogapp.blogapp.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.myblogapp.blogapp.Adapters.UsersAdapter;
import com.myblogapp.blogapp.R;

/**
 * Created by denmariebalingit on 3/30/17.
 */

public class UsersListActivity extends AppCompatActivity {

  @Bind(R.id.userRecycler) RecyclerView userRecycler;

  private UsersAdapter usersAdapter;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.user_lists_activity);
    ButterKnife.bind(this);


    usersAdapter = new UsersAdapter(this);
    userRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    userRecycler.setAdapter(usersAdapter);
  }
}
