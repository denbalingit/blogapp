package com.myblogapp.blogapp.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.myblogapp.blogapp.Models.BlogEntry;
import com.myblogapp.blogapp.R;
import com.myblogapp.blogapp.Utils.MyLogger;
import com.myblogapp.blogapp.Utils.RealmController;
import com.myblogapp.blogapp.Utils.UserUtil;

/**
 * Created by denmariebalingit on 3/30/17.
 */

public class AddBlogActivity extends AppCompatActivity implements View.OnClickListener {

  @Bind(R.id.name) EditText name;
  @Bind(R.id.date) EditText date;
  @Bind(R.id.content) EditText content;
  @Bind(R.id.uploadImageBtn) Button uploadImageBtn;
  @Bind(R.id.createBtn) Button createBtn;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.add_blog_activity);
    ButterKnife.bind(this);

    uploadImageBtn.setOnClickListener(this);
    createBtn.setOnClickListener(this);

    String blogTitle = getIntent().getStringExtra("title");
    BlogEntry blogEntry = RealmController.getInstance().getRealm().where(BlogEntry.class).equalTo("title",blogTitle).findFirst();

  }

  @Override public void onClick(View view) {
    switch (view.getId()){

      case R.id.uploadImageBtn :
        //upload image
        for(BlogEntry blogEntry : UserUtil.getInstance().getLoggedUser().getBlogEntries()){
          MyLogger.showLog("blogEntry name : "+blogEntry.getName());
        }
        break;
      case R.id.createBtn :
        createBlog();
        break;

    }
  }

  private void createBlog(){

    BlogEntry newBlogEntry = new BlogEntry();

    newBlogEntry.setName(name.getText().toString());
    newBlogEntry.setDate(date.getText().toString());
    newBlogEntry.setContent(content.getText().toString());
    newBlogEntry.setActive(true);

    //temp only
    newBlogEntry.setImagePath(name.getText().toString());

    newBlogEntry.setAuthor(UserUtil.getInstance().getLoggedUser());


    RealmController.getInstance().save(newBlogEntry);

    MyLogger.showLog("author : "+UserUtil.getInstance().getLoggedUser());

  }

  //private void addUserToBlockList(User user){
  //
  //  RealmController.getInstance().save(UserUtil.getInstance().getLoggedUser().getBlockUserList().add(user);
  //
  //
  //}



}
