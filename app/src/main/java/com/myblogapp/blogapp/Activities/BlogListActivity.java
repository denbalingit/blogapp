package com.myblogapp.blogapp.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.myblogapp.blogapp.Models.User;
import com.myblogapp.blogapp.R;
import com.myblogapp.blogapp.Utils.MyLogger;
import com.myblogapp.blogapp.Utils.UserUtil;

/**
 * Created by denmariebalingit on 3/30/17.
 */

public class BlogListActivity extends AppCompatActivity implements View.OnClickListener {

  @Bind(R.id.fab) FloatingActionButton fab;
  @Bind(R.id.viewUsersBtn) Button viewUserBtn;


  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.blog_list_activity);
    ButterKnife.bind(this);

    viewUserBtn.setOnClickListener(this);
    User loggedUser = UserUtil.getInstance().getLoggedUser();
    MyLogger.showLog("size : "+loggedUser.getBlockUserList().size());

    for(User user :loggedUser.getBlockUserList()){
      MyLogger.showLog("block users are : "+user.getName());

    }



    fab.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View view) {
        startActivity(new Intent(BlogListActivity.this, AddBlogActivity.class));
      }
    });

  }

  @Override public void onClick(View view) {
    switch (view.getId()){
      case R.id.viewUsersBtn :
        startActivity(new Intent(this,UsersListActivity.class));
        break;
    }
  }
}
